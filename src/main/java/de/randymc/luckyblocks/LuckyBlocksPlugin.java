package de.randymc.luckyblocks;

import de.randymc.luckyblocks.listener.SpongeBreakListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class LuckyBlocksPlugin extends JavaPlugin {

	@Override
	public void onEnable() {
		this.getServer().getLogger().info("Das Plugin wurde aktiviert.");
		this.registerListeners();
	}

	@Override
	public void onDisable() {
		this.getServer().getLogger().info("Das Plugin wurde deaktiviert.");
	}

	private void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new SpongeBreakListener(), this);
	}

}
