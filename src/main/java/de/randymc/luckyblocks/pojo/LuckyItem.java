package de.randymc.luckyblocks.pojo;

import lombok.Data;
import org.bukkit.inventory.ItemStack;

@Data
public class LuckyItem {

	ItemStack itemStack;

	int probability;

}
