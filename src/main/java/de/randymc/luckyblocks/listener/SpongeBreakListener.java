package de.randymc.luckyblocks.listener;

import de.randymc.luckyblocks.pojo.LuckyItem;
import de.randymc.luckyblocks.service.ItemService;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class SpongeBreakListener implements Listener {

	private ItemService itemService;

	public SpongeBreakListener() {
		this.itemService = new ItemService();
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		if (!e.getBlock().getType().equals(Material.SPONGE)) {
			return;
		}

		Location blockLocation = e.getBlock().getLocation();
		World blockWorld = blockLocation.getWorld();

		LuckyItem randomlySelectedItem = itemService.getRandomItem();
		blockWorld.dropItemNaturally(blockLocation, randomlySelectedItem.getItemStack());
	}

}
