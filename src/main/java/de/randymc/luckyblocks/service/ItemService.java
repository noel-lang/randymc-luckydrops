package de.randymc.luckyblocks.service;

import de.randymc.luckyblocks.pojo.LuckyItem;
import lombok.Data;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Data
public class ItemService {

	private List<LuckyItem> items = new ArrayList<>();
	private Random random = new Random();
	int totalSum = 0;

	public ItemService() {
		addItems();
		for (LuckyItem item : getItems()) {
			totalSum = getTotalSum() + item.getProbability();
		}
	}

	public LuckyItem getRandomItem() {
		int index = random.nextInt(totalSum);
		int sum = 0;
		int i = 0;

		while (sum < index) {
			sum = sum + items.get(i++).getProbability();
		}

		return items.get(Math.max(0, i - 1));
	}

	private void addItems() {
		// Eisenerz
		items.add(generateSimpleLuckyItem(Material.COAL_ORE, 3));
		items.add(generateSimpleLuckyItem(Material.IRON_ORE, 3));
		items.add(generateSimpleLuckyItem(Material.GOLD_ORE, 2));
		items.add(generateSimpleLuckyItem(Material.DIAMOND_ORE, 1));

		// Essen
		items.add(generateSimpleLuckyItem(Material.APPLE, 2));
		items.add(generateSimpleLuckyItem(Material.COOKED_BEEF, 2));
		items.add(generateSimpleLuckyItem(Material.COOKED_CHICKEN, 2));
	}

	private LuckyItem generateSimpleLuckyItem(Material material, int probability) {
		LuckyItem luckySimpleItem = new LuckyItem();
		ItemStack ironItemStack = new ItemStack(material, 1);
		luckySimpleItem.setItemStack(ironItemStack);
		luckySimpleItem.setProbability(probability);

		return luckySimpleItem;
	}

}
